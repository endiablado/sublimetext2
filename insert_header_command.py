import sublime, sublime_plugin

class InsertHeaderCommand(sublime_plugin.TextCommand):

    def run(self, edit):
        regions = []
        # Loop through each cursor
        for pos in self.view.sel():
            # Get the position of the given cursor
            line, col = self.view.rowcol(pos.begin())
            # Assemble the header comment starting at the column provided
            comment = '/' + '*' * (77 - col)
            comment += '\n' + ' ' * col
            comment += '\n' + ' ' * col
            comment += '*' * (77 - col) + '/'
            # Insert the comment header
            self.view.insert(edit, pos.begin(), comment)
            # Store the new position
            regions.append( self.view.text_point(line + 1, col) )
        # clear the cursors
        self.view.sel().clear()
        # Add new cursors from the stored positions
        for region in regions:
            self.view.sel().add( sublime.Region(region) )
